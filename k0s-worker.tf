resource "aws_network_interface" "worker1_nic" {
  subnet_id       = aws_subnet.k0s_subnet["eu-central-1b"].id
  security_groups = [aws_security_group.k0s-workers.id]
  tags            = var.common_tags
}
resource "aws_eip" "worker1_pip" {
  instance = aws_instance.worker1.id
  vpc      = true
  associate_with_private_ip = aws_network_interface.worker1_nic.private_ip
}

resource "aws_instance" "worker1" {
  ami                         = var.ami
  instance_type               = "t2.micro"
  key_name                    = "david"
  network_interface {
    network_interface_id = aws_network_interface.worker1_nic.id
    device_index         = 0
  }
}

resource "aws_network_interface" "worker2_nic" {
  subnet_id       = aws_subnet.k0s_subnet["eu-central-1c"].id
  security_groups = [aws_security_group.k0s-workers.id]
  tags            = var.common_tags
}

resource "aws_eip" "worker2_pip" {
  instance = aws_instance.worker2.id
  vpc      = true
  associate_with_private_ip = aws_network_interface.worker2_nic.private_ip
}

resource "aws_instance" "worker2" {
  ami                         = var.ami
  instance_type               = "t2.micro"
  key_name                    = "david"
  network_interface {
    network_interface_id = aws_network_interface.worker2_nic.id
    device_index         = 0
  }
}

output "worker1_vm_private_ip" {
  value = aws_network_interface.worker1_nic.private_ip
}

output "worker2_vm_private_ip" {
  value = aws_network_interface.worker2_nic.private_ip
}