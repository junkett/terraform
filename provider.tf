terraform {
  backend "s3" {
    bucket = "jnkpnktfstate"
    key    = "tfstate/tatum.tfstate"
    region = "eu-central-1"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.region
}
