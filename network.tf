# Create a VPC
resource "aws_vpc" "k0s_vpc" {
  cidr_block = var.vpc_address
  tags       = var.common_tags
}

# Subnets
resource "aws_subnet" "k0s_subnet" {
  for_each   = var.subnets
  vpc_id     = aws_vpc.k0s_vpc.id
  cidr_block = each.value
  tags       = var.common_tags
  availability_zone = each.key
}

resource "aws_security_group" "k0s-controller" {
  name        = "k0s-controller"
  description = "K0S Controller security group"
  vpc_id      = aws_vpc.k0s_vpc.id

  ingress {
    description = "inbound k0s controller"
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Anything from controller"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.1.0.0/24","10.1.1.0/24","10.1.2.0/24"]
  }
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Outbound allowed to internet"
  }

  tags = var.common_tags
}

resource "aws_security_group" "k0s-workers" {
  name        = "k0s-workers"
  description = "K0S Workers security group"
  vpc_id      = aws_vpc.k0s_vpc.id

  ingress {
    description = "Anything from controller"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.1.0.0/24","10.1.1.0/24","10.1.2.0/24"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Outbound allowed to internet"
  }

  tags = var.common_tags
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.k0s_vpc.id

  tags = var.common_tags
}

resource "aws_main_route_table_association" "k0s2gw" {
  vpc_id         = aws_vpc.k0s_vpc.id
  route_table_id = aws_route_table.rt.id
}

resource "aws_route_table" "rt" {
  vpc_id = aws_vpc.k0s_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  tags = var.common_tags
}