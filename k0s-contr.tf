# Linux
resource "aws_network_interface" "contr_nic" {
  subnet_id       = aws_subnet.k0s_subnet["eu-central-1a"].id
  security_groups = [aws_security_group.k0s-controller.id]

  tags = var.common_tags
}

resource "aws_eip" "contr_pip" {
  instance = aws_instance.contr.id
  vpc      = true
  associate_with_private_ip = aws_network_interface.contr_nic.private_ip
}

resource "aws_instance" "contr" {
  ami                         = var.ami
  instance_type               = "t2.micro"
  availability_zone           = "eu-central-1a"
  key_name                    = "david"
  network_interface {
    network_interface_id = aws_network_interface.contr_nic.id
    device_index         = 0
  }
}

output "contr_vm_public_ip" {
  value = aws_eip.contr_pip.public_ip
}

output "contr_vm_private_ip" {
  value = aws_network_interface.contr_nic.private_ip
}

output "contr_vm_public_name" {
  value = aws_eip.contr_pip.public_dns
}
