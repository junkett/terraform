resource "aws_s3_bucket" "datastore" {
  bucket = "jnkdata"
  acl    = "private"

  tags = var.common_tags
}

resource "aws_s3_bucket" "web" {
  bucket = "pocasi.bartosovic.cz"
  acl    = "public-read"

  website {
    index_document = "index.html"
  }
  tags = var.common_tags
}

resource "aws_s3_bucket_policy" "public_web" {
  bucket = aws_s3_bucket.web.id
  policy = <<EOT
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "${aws_s3_bucket.web.arn}/*"
            ]
        }
    ]
}
EOT
}
