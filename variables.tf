variable "vpc_address" {
  default = "10.1.0.0/21"
}

variable "region" {
  default = "eu-central-1"
}

variable "common_tags" {
  default = {
    Name        = "pocasi-infra"
    Environment = "dev"
    Region      = "eu-central-1"
    Owner       = "David Bartos"
  }
}

variable "ami" {
  default = "ami-0a49b025fffbbdac6"
}

variable "subnets" {
  default = {
    eu-central-1a = "10.1.0.0/24"
    eu-central-1b = "10.1.1.0/24"
    eu-central-1c = "10.1.2.0/24"
  }
}